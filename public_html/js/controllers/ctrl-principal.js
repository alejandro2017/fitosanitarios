var app = angular.module("myApp", ["ngAnimate", "ui.bootstrap"]);

var jsCombo = [
    {"id": "Valor1", "value": ["V1", "V2"]},
    {"id": "Valor2", "value": ["X1", "X2"]}
];
app.controller("ctrlPrincipal", ["$scope",
    function (o) {
        o.today = function () {
            o.frm.fcIns = new Date();
            o.frm.fcTra = new Date();
        };
   

        o.frm = {"jsCombo": jsCombo, "cmb1": "Valor1", "cmb2": "V1"};
        o.encontrarPorIdCombo = function (id, arreglo) {
            var r = [];
            var max = arreglo.length;
            for (var i = 0; i < max; i++) {
                var it = arreglo[i];
                if (it.id === id) {
                    r = it.value;
                    break;
                }
            }
            return r;
        };
        o.cambiarCombo1 = function (val) {
            var vl = o.encontrarPorIdCombo(val, jsCombo)[0];
            o.frm.cmb2 = vl;
        };
             o.today();

    }]);    